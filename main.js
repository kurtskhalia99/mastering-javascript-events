document.addEventListener("DOMContentLoaded", function(){
    let newSection = document.createElement('section');
    newSection.classList.add('MyClass');
    let main = document.querySelector("#app-container");
    let footer = document.querySelector(".app-footer");
    main.insertBefore(newSection,footer);


    let title = document.createElement('h2');
    title.classList.add('app-title');
    let textnode1 = document.createTextNode('Join Our Program');
    title.appendChild(textnode1); 
    newSection.appendChild(title);


    let subTitle = document.createElement('h3');
    subTitle.classList.add('app-subtitle');
    let br = document.createElement('br');
    let textnode2 = document.createTextNode('Sed do eiusmod tempor incididunt ut');
    let textnode3 = document.createTextNode('labore et dolore magna aliqua.');
    subTitle.appendChild(textnode2); 
    subTitle.appendChild(br); 
    subTitle.appendChild(textnode3); 
    newSection.appendChild(subTitle);


    let form = document.createElement('form');
    newSection.appendChild(form);



    let input = document.createElement('input');
    input.classList.add('app-input');
    input.placeholder = 'Email';
    input.setAttribute("type", "email");
    form.appendChild(input);

    let button = document.createElement('button');
    button.innerHTML = "SUBSCRIBE";
    button.classList.add('app-section__button');
    button.classList.add('app-section__button--read-more');
    button.setAttribute("type", "submit");
    form.appendChild(button);

    form.addEventListener('submit', logSubmit);
    function logSubmit(e) {
        console.log(input.value);
        e.preventDefault();
    }
    
});